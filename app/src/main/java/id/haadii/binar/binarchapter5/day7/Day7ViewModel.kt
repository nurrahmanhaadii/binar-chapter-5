package id.haadii.binar.binarchapter5.day7

import android.util.Log
import androidx.lifecycle.*
import id.haadii.binar.binarchapter5.BuildConfig
import id.haadii.binar.binarchapter5.model.MovieResponse
import id.haadii.binar.binarchapter5.service.TMDBApiService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by nurrahmanhaadii on 20,April,2022
 */
class Day7ViewModel(private val apiService: TMDBApiService) : ViewModel() {

    // di gunakan untuk assign value, hanya untuk di dalam view model
    private val _dataSuccess = MutableLiveData<MovieResponse>()
    // di gunakan untuk observe di fragment/activity
    val dataSuccess : LiveData<MovieResponse> get() = _dataSuccess

    private val _dataError = MutableLiveData<String>()
    val dataError : LiveData<String> get() = _dataError

    fun getAllMovie() {
        apiService.getAllMovie(BuildConfig.API_KEY)
            .enqueue(object : Callback<MovieResponse> {
                // kondisi get data berhasil dari http
                override fun onResponse(
                    call: Call<MovieResponse>,
                    response: Response<MovieResponse>
                ) {
                    // response.issuccessful sama dengan 200
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            _dataSuccess.postValue(response.body())
                        } else {
                            _dataError.postValue("Datanya kosong")
                        }
                    } else {
                        _dataError.postValue("Pengambilan data gagal}")
                    }
                    Log.e("onResponse", "test")
                }
                // kondisi get data gagal dari server/http, udh bener2 ga bisa di akses
                override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                    _dataError.postValue("Server bermasalah")
                    Log.e("onFailure", "test")
                }

            })
    }
}