package id.haadii.binar.binarchapter5.day9

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import id.haadii.binar.binarchapter5.R
import id.haadii.binar.binarchapter5.databinding.FragmentDay9Binding
import id.haadii.binar.binarchapter5.day8.Day8ViewModel
import id.haadii.binar.binarchapter5.helper.viewModelsFactory
import id.haadii.binar.binarchapter5.service.TMDBApiService
import id.haadii.binar.binarchapter5.service.TMDBClient
import java.text.SimpleDateFormat
import java.util.*


class Day9Fragment : Fragment() {
    private var _binding : FragmentDay9Binding? = null
    private val binding get() = _binding!!

    private val apiService: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: Day8ViewModel by viewModelsFactory { Day8ViewModel(apiService) }

    private val cal = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay9Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val idMovie = arguments?.getInt("id_movie")
        Toast.makeText(requireContext(), idMovie.toString(), Toast.LENGTH_SHORT).show()
        viewModel.getDetailMovie(idMovie ?: 0)
        binding.tvDateDialog.setOnClickListener {
            createDateDialog().show()
        }
        binding.etDateOfBirth.setOnClickListener {
            createDateDialog().show()
        }
    }

    private fun createDateDialog(): DatePickerDialog {
        return DatePickerDialog(requireContext(),
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH))
    }

    // create an OnDateSetListener
    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "MM/dd/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            val stringDate = sdf.format(cal.time)
            Toast.makeText(requireContext(), stringDate, Toast.LENGTH_SHORT).show()
            binding.etDateOfBirth.setText(stringDate)
        }

}