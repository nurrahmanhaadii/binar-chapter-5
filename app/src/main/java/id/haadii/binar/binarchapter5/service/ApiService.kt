package id.haadii.binar.binarchapter5.service

import androidx.lifecycle.LiveData
import id.haadii.binar.binarchapter5.model.CarItem
import id.haadii.binar.binarchapter5.model.RegisterRequest
import id.haadii.binar.binarchapter5.model.RegisterResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


/**
 * Created by nurrahmanhaadii on 14,April,2022
 */
interface ApiService {

    /**
     * untuk mendapatkan list semua mobil
     */
    @GET("/admin/car")
    fun getAllCar() : Call<List<CarItem>>

    @POST("/admin/auth/register")
    fun registerAdmin(@Body request: RegisterRequest) : Call<RegisterResponse>

    @GET("/admin/car")
    fun getAllCarLiveData() : LiveData<List<CarItem>>

}