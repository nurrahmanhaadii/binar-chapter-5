package id.haadii.binar.binarchapter5.day4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import id.haadii.binar.binarchapter5.databinding.ActivityDay4Binding
import id.haadii.binar.binarchapter5.helper.StudentRepo
import id.haadii.binar.binarchapter5.helper.viewModelsFactory
import id.haadii.binar.binarchapter5.service.ApiClient
import id.haadii.binar.binarchapter5.service.ApiService

class Day4Activity : AppCompatActivity() {

    private lateinit var binding: ActivityDay4Binding
    private val studentRepo : StudentRepo by lazy { StudentRepo(this) }
    private val apiService : ApiService by lazy { ApiClient.instance }

    private val viewModel: Day4ViewModel by viewModelsFactory { Day4ViewModel(studentRepo, apiService) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDay4Binding.inflate(layoutInflater)
        setContentView(binding.root)

//        withoutViewModel()
        withViewModel()
//        viewModel.getData()
//        observeData()
    }

    /**
     * Start Region Tanpa ViewModel
     */

    private fun withoutViewModel() {
        buttonPlusClicked()
        buttonMinusClicked()
    }

    private fun buttonPlusClicked() {
        binding.btnPlus.setOnClickListener {
            val newText = binding.tvCount.text.toString().toInt().plus(1)
            updateText(newText)
        }
    }

    private fun buttonMinusClicked() {
        binding.btnMinus.setOnClickListener {
            val newText = binding.tvCount.text.toString().toInt().minus(1)
            updateText(newText)
        }
    }

    /**
     * End Region Tanpa ViewModel
     */

    /**
     * Start Region Dengan ViewModel
     */

    private fun withViewModel() {
        binding.btnPlus.setOnClickListener {
            viewModel.increment()
        }

        binding.btnMinus.setOnClickListener {
            viewModel.decrement()
        }

        viewModel.nilaiBaru.observe(this) {
            updateText(it)
        }
    }

    /**
     * End Region Dengan ViewModel
     */

    private fun updateText(newValue: Int) {
        binding.tvCount.text = newValue.toString()
    }

    private fun observeData() {
        viewModel.students.observe(this) {
            Toast.makeText(this, "${it.size}", Toast.LENGTH_SHORT).show()
        }
    }

}