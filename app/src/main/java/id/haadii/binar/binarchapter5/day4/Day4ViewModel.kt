package id.haadii.binar.binarchapter5.day4

import androidx.lifecycle.*
import id.haadii.binar.binarchapter5.helper.StudentRepo
import id.haadii.binar.binarchapter5.model.CarItem
import id.haadii.binar.binarchapter5.service.ApiService
import id.haadii.binar.chapter4.database.Student
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by nurrahmanhaadii on 15,April,2022
 */
class Day4ViewModel(private val studentRepo: StudentRepo, private val apiService: ApiService) : ViewModel() {

    val nilaiBaru = MutableLiveData<Int>(0)
    val students = MutableLiveData<List<Student>>()
    val addData = MutableLiveData<Boolean>()

    private val _cars = MutableLiveData<List<CarItem>>()
    val cars : LiveData<List<CarItem>> get() = _cars

    var errorMessage = MutableLiveData<String>()
    val studentItem = MutableLiveData<Student>()

    val testPostValue = MutableLiveData<String>()
    val testSetValue = MutableLiveData<String>()

    val mediator = MediatorLiveData<String>()


    fun increment() {
        nilaiBaru.value = nilaiBaru.value?.plus(1)
    }

    fun decrement() {
        nilaiBaru.value = nilaiBaru.value?.minus(1)
    }

    fun testSetValue() {
        testSetValue.postValue("1")
        testSetValue.value = "2"
    }

    fun testPostValue() {
        viewModelScope.launch {
            testPostValue.value = "satu"
            testPostValue.postValue("dua")
        }
    }

    fun testMediator() {
        mediator.addSource(testSetValue) {
            mediator.value = it
        }
        mediator.addSource(testPostValue) {
            mediator.value = it
        }
    }

    // Get data dari database room
    fun getData() {
        // viewmodel scope penggunaannya mirip coroutinescope
        viewModelScope.launch {
            students.value = studentRepo.getDataStudent()
        }
    }

    fun addData() {
        viewModelScope.launch {
            val student = Student(null, "nama saya", "email saya", "password", "")
            studentRepo.insertStudent(student)
            addData.value = true
        }
    }

    fun updateData(existingStudent: Student, username: String, nama: String, tglLahir: String, alamat: String) {
        viewModelScope.launch {
            val studentUpdate =
                Student(id = existingStudent.id, email = existingStudent.email, password = existingStudent.password,
                    dateOfBirth = tglLahir, userName = username
                )
            studentRepo.updateStudent(studentUpdate)
        }
    }

    fun getDataFromNetwork() {
        apiService.getAllCar().enqueue(object : Callback<List<CarItem>> {
            override fun onResponse(call: Call<List<CarItem>>, response: Response<List<CarItem>>) {
                if (response.isSuccessful) {
                    _cars.value = response.body()
                } else {
                    val message = "gagal dapat data"
                    errorMessage.value = message
                }
            }

            override fun onFailure(call: Call<List<CarItem>>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }
}