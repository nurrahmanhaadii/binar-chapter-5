package id.haadii.binar.binarchapter5.day3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.haadii.binar.binarchapter5.R
import id.haadii.binar.binarchapter5.adapter.CarAdapter
import id.haadii.binar.binarchapter5.databinding.FragmentDay3Binding
import id.haadii.binar.binarchapter5.databinding.LayoutDialogAddBinding
import id.haadii.binar.binarchapter5.model.CarItem
import id.haadii.binar.binarchapter5.model.RegisterRequest
import id.haadii.binar.binarchapter5.model.RegisterResponse
import id.haadii.binar.binarchapter5.service.ApiClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Day3Fragment : Fragment() {

    private var _binding: FragmentDay3Binding? = null
    private val binding get() = _binding!!

    private lateinit var carAdapter: CarAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        getDataFromNetWork()
        onAddClicked()
    }

    private fun initRecyclerView() {
        carAdapter = CarAdapter { id: Int, car: CarItem ->
            val bundle = Bundle()
            bundle.putInt("id", id)
            findNavController().navigate(R.id.action_day8Fragment_to_pageTwoFragment, bundle)
        }
        binding.rvCar.apply {
            adapter = carAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun getDataFromNetWork() {
        val apiService = ApiClient.instance
        apiService.getAllCar().enqueue(object : Callback<List<CarItem>> {
            override fun onResponse(call: Call<List<CarItem>>, response: Response<List<CarItem>>) {
                // isSuccessful = code() == 200
                if (response.isSuccessful) {
                    if (!response.body().isNullOrEmpty()) {
                        if (response.body() != null) {
                            carAdapter.updateData(response.body()!!)
                        }
                        response.body()?.let { carAdapter.updateData(it) }
                    }
                }
                binding.pbCar.isVisible = false
            }

            override fun onFailure(call: Call<List<CarItem>>, t: Throwable) {
                binding.pbCar.isVisible = false
            }

        })
    }

    private fun onAddClicked() {
        binding.fabAdd.setOnClickListener {
            createCustomDialog { email, password ->
                binding.pbCar.isVisible = true
                registerNewAdmin(email, password)
            }
        }
    }

    private fun createCustomDialog(onClickListener: (email: String, password: String) -> Unit) {
        val binding = LayoutDialogAddBinding.inflate(LayoutInflater.from(requireContext()), null, false)
        val customLayout = binding.root

        val builder = AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        binding.apply {
            btnSave.setOnClickListener {
                onClickListener.invoke(etEmail.text.toString(), etPassword.text.toString())
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    private fun registerNewAdmin(email: String, password: String) {
        val apiService = ApiClient.instance
        val request = RegisterRequest(email = email, password = password, role = "admin")
        apiService.registerAdmin(request).enqueue(object : Callback<RegisterResponse> {
            override fun onResponse(
                call: Call<RegisterResponse>,
                response: Response<RegisterResponse>
            ) {
                if (response.isSuccessful) {
                    response.body()
                    Toast.makeText(requireContext(), "Register Success", Toast.LENGTH_SHORT).show()
                } else {
                    val objError = JSONObject(response.errorBody()!!.string())
                    val message = objError.getString("message")
                    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
                }
                binding.pbCar.isVisible = false
            }

            override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                binding.pbCar.isVisible = false
            }

        })
    }
}