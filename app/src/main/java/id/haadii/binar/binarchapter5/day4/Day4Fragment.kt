package id.haadii.binar.binarchapter5.day4

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import id.haadii.binar.binarchapter5.databinding.FragmentDay4Binding
import id.haadii.binar.binarchapter5.helper.StudentRepo
import id.haadii.binar.binarchapter5.helper.viewModelsFactory
import id.haadii.binar.binarchapter5.service.ApiClient
import id.haadii.binar.binarchapter5.service.ApiService


class Day4Fragment : Fragment() {

    private var _binding: FragmentDay4Binding? = null
    private val binding get() = _binding!!

    private val studentRepo : StudentRepo by lazy { StudentRepo(requireContext()) }

    private val apiService : ApiService by lazy { ApiClient.instance }

    private val viewModel: Day4ViewModel by viewModelsFactory { Day4ViewModel(studentRepo, apiService) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay4Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        withViewModel()
        viewModel.addData()
        viewModel.getDataFromNetwork()
        viewModel.testSetValue()
        viewModel.testPostValue()
        viewModel.testMediator()
        observeData()
    }

    private fun withViewModel() {
        binding.btnPlus.setOnClickListener {
            viewModel.increment()
        }

        binding.btnMinus.setOnClickListener {
            viewModel.decrement()
        }

        viewModel.nilaiBaru.observe(requireActivity()) {
            updateText(it)
        }
    }

    /**
     * End Region Dengan ViewModel
     */

    private fun updateText(newValue: Int) {
        binding.tvCount.text = newValue.toString()
    }

    private fun observeData() {
        viewModel.addData.observe(requireActivity()) {
            // buat munculin data dengan manggil data dari database
            viewModel.getData()
        }
        viewModel.students.observe(requireActivity()) {
            Snackbar.make(binding.root, "${it.size}", Snackbar.LENGTH_SHORT).show()
        }
        viewModel.cars.observe(requireActivity()) {
            Toast.makeText(requireContext(), "CAR : ${it.size}", Toast.LENGTH_SHORT).show()
        }
        viewModel.errorMessage.observe(requireActivity()) {

        }

        viewModel.studentItem.observe(requireActivity()) {

        }

        viewModel.testSetValue.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

        viewModel.testPostValue.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

        viewModel.mediator.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
    }

}