package id.haadii.binar.binarchapter5.service

import androidx.lifecycle.LiveData
import id.haadii.binar.binarchapter5.model.DetailMovieResponse
import id.haadii.binar.binarchapter5.model.MovieResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


/**
 * Created by nurrahmanhaadii on 20,April,2022
 */
interface TMDBApiService {

    @GET("movie/popular")
    fun getAllMovie(@Query("api_key") key: String) : Call<MovieResponse>

    @GET("movie/{id}")
    fun getDetailMovie(@Path("id") movieId: Int, @Query("api_key") key: String) : Call<DetailMovieResponse>
}