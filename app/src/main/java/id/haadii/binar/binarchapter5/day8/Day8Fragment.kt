package id.haadii.binar.binarchapter5.day8

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.snackbar.Snackbar
import id.haadii.binar.binarchapter5.BuildConfig
import id.haadii.binar.binarchapter5.R
import id.haadii.binar.binarchapter5.databinding.FragmentDay8Binding
import id.haadii.binar.binarchapter5.day4.Day4ViewModel
import id.haadii.binar.binarchapter5.day7.Day7ViewModel
import id.haadii.binar.binarchapter5.helper.StudentRepo
import id.haadii.binar.binarchapter5.helper.toDate
import id.haadii.binar.binarchapter5.helper.toHourMinutes
import id.haadii.binar.binarchapter5.helper.viewModelsFactory
import id.haadii.binar.binarchapter5.service.ApiClient
import id.haadii.binar.binarchapter5.service.ApiService
import id.haadii.binar.binarchapter5.service.TMDBApiService
import id.haadii.binar.binarchapter5.service.TMDBClient


class Day8Fragment : Fragment() {
    private var _binding: FragmentDay8Binding? = null
    private val binding get() = _binding!!

    private val apiService: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: Day8ViewModel by viewModelsFactory { Day8ViewModel(apiService) }

    private val studentRepo : StudentRepo by lazy { StudentRepo(requireContext()) }
    private val api: ApiService by lazy { ApiClient.instance }
    private val day4ViewModel: Day4ViewModel by viewModelsFactory { Day4ViewModel(studentRepo, api) }

    private var idMovie : Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay8Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllMovie()
        day4ViewModel.getData()
        movePage()
        observeData()
    }

    private fun movePage() {
        binding.btnMove.setOnClickListener {
            findNavController().navigate(R.id.action_day8Fragment_to_pageTwoFragment)
        }

        binding.btnMoveTo9.setOnClickListener {
            if (idMovie == null) {
                Toast.makeText(requireContext(), "Id movie masih null, tunggu sampai get data selesai", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val mBundle = Bundle()
            mBundle.putInt("id_movie", idMovie ?: 0)
            findNavController().navigate(R.id.action_day8Fragment_to_day9Fragment, mBundle)

        }
    }

    private fun observeData() {
        viewModel.dataSuccess.observe(viewLifecycleOwner) {
            idMovie = it.results[0].id

            val duration = 61.toHourMinutes()
            val date = it.results[0].releaseDate.toDate()
            binding.tvText.text = duration
            val voteAverage = (it.results[0].voteAverage * 10).toInt()
            binding.determinateBar.progress = voteAverage
            binding.tvPercentage.text = "$voteAverage%"

            val urlImage = "${BuildConfig.BASE_URL_IMAGE}${it.results[2].posterPath}"
            val bgOptions = RequestOptions()
                .placeholder(R.drawable.ic_baseline_android_24)
            Glide.with(requireContext())
                .load(urlImage)
                .apply(bgOptions)
                .into(object : CustomTarget<Drawable?>() {
                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable?>?) {
                        binding.ivPoster.setImageDrawable(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {

                    }
                })
        }

        viewModel.dataError.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

        day4ViewModel.students.observe(viewLifecycleOwner) {
            Snackbar.make(binding.root, it.size.toString(), Snackbar.LENGTH_SHORT).show()
        }
    }

}