package id.haadii.binar.binarchapter5.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import id.haadii.binar.binarchapter5.R
import id.haadii.binar.binarchapter5.databinding.ItemCarBinding
import id.haadii.binar.binarchapter5.model.Car
import id.haadii.binar.binarchapter5.model.CarItem


/**
 * Created by nurrahmanhaadii on 14,April,2022
 */

// adapter wajib extend recyclerview adapter
class CarAdapter(private val onClickListener : (id: Int, car: CarItem) -> Unit) : RecyclerView.Adapter<CarAdapter.CarViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<CarItem>() {
        override fun areItemsTheSame(oldItem: CarItem, newItem: CarItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CarItem, newItem: CarItem): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val listDiffer = AsyncListDiffer(this, diffCallback)

    fun updateData(cars: List<CarItem>) = listDiffer.submitList(cars)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val binding = ItemCarBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CarViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.bind(listDiffer.currentList[position])
    }

    override fun getItemCount(): Int = listDiffer.currentList.size

    /**
     * view holder wajib extend RecyclerView ViewHolder
     * ViewHolder butuh view maka kita tambahkan parameter view
     *
     * untuk view binding
     * binding.root == view
     * jd kita bisa mengganti view dengan binding.root
     */
    inner class CarViewHolder(private val binding: ItemCarBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CarItem) {
            binding.apply {
                tvCarName.text = item.name

                val bgOptions = RequestOptions()
                    .placeholder(R.drawable.ic_add)
                Glide.with(itemView.context)
                    .load(item.image)
                    .apply(bgOptions)
                    .centerCrop()
                    .into(object : CustomTarget<Drawable?>() {
                        override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable?>?) {
                            ivCar.setImageDrawable(resource)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }
                    })

                binding.tvCarName.setOnClickListener {
                    onClickListener(item.id, item)
                }
//
//
//                Glide.with(itemView.context)
//                    .load(item.image)
//                    .into(ivCar)
            }
        }
    }

}