package id.haadii.binar.binarchapter5.day8

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import id.haadii.binar.binarchapter5.R
import id.haadii.binar.binarchapter5.databinding.FragmentPageTwoBinding
import id.haadii.binar.binarchapter5.helper.viewModelsFactory
import id.haadii.binar.binarchapter5.service.TMDBApiService
import id.haadii.binar.binarchapter5.service.TMDBClient


class PageTwoFragment : Fragment() {

    private var _binding: FragmentPageTwoBinding? = null
    private val binding get() = _binding!!

    private val apiService: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: Day8ViewModel by viewModelsFactory { Day8ViewModel(apiService) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPageTwoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllMovie()
        observeData()
    }

    private fun observeData() {
        viewModel.dataSuccess.observe(viewLifecycleOwner) {
            Snackbar.make(binding.root, it.results[0].title, Snackbar.LENGTH_SHORT).show()
            binding.tvText.text = it.results[0].title
        }

        viewModel.dataError.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
    }

}