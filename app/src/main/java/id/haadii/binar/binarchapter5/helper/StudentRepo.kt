package id.haadii.binar.binarchapter5.helper

import android.content.Context
import android.widget.TextView
import id.haadii.binar.chapter4.database.Student
import id.haadii.binar.chapter4.database.StudentDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 * Created by nurrahmanhaadii on 07,April,2022
 */
class StudentRepo(context: Context) {

    private val mDb = StudentDatabase.getInstance(context)

    suspend fun getDataStudent() = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.getAllStudent()
    }

    suspend fun updateStudent(student: Student) = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.updateStudent(student)
    }

    suspend fun insertStudent(student: Student) = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.insertStudent(student)
    }

    suspend fun deleteStudent(student: Student) = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.deleteStudent(student)
    }

}