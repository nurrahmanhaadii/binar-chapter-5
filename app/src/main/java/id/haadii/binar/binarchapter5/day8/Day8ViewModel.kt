package id.haadii.binar.binarchapter5.day8

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.haadii.binar.binarchapter5.BuildConfig
import id.haadii.binar.binarchapter5.model.DetailMovieResponse
import id.haadii.binar.binarchapter5.model.MovieResponse
import id.haadii.binar.binarchapter5.service.TMDBApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by nurrahmanhaadii on 21,April,2022
 */
class Day8ViewModel(private val apiService: TMDBApiService) : ViewModel() {

    // di gunakan untuk assign value, hanya untuk di dalam view model
    private val _dataSuccess = MutableLiveData<MovieResponse>()

    // di gunakan untuk observe di fragment/activity
    val dataSuccess: LiveData<MovieResponse> get() = _dataSuccess

    private val _dataError = MutableLiveData<String>()
    val dataError: LiveData<String> get() = _dataError

    fun getAllMovie() {
        apiService.getAllMovie(BuildConfig.API_KEY)
            .enqueue(object : Callback<MovieResponse> {
                // kondisi get data berhasil dari http
                override fun onResponse(
                    call: Call<MovieResponse>,
                    response: Response<MovieResponse>
                ) {
                    // response.issuccessful sama dengan 200
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            _dataSuccess.postValue(response.body())
                        } else {
                            _dataError.postValue("Datanya kosong")
                        }
                    } else {
                        _dataError.postValue("Pengambilan data gagal}")
                    }
                    Log.e("onResponse", "test")
                }
                // kondisi get data gagal dari server/http, udh bener2 ga bisa di akses
                override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                    _dataError.postValue("Server bermasalah")
                    Log.e("onFailure", "test")
                }

            })
    }

    fun getDetailMovie(movieId: Int) {
        apiService.getDetailMovie(movieId, BuildConfig.API_KEY)
            .enqueue(object : Callback<DetailMovieResponse> {
                override fun onResponse(
                    call: Call<DetailMovieResponse>,
                    response: Response<DetailMovieResponse>
                ) {
                    Log.e("test", "test")
                }

                override fun onFailure(call: Call<DetailMovieResponse>, t: Throwable) {
                    Log.e("test", "test")
                }

            })
    }

}